package com.mark.tool.bloomFilter.bitMap;
/**
 * 类型：32位和64位
 * @author maliqiang
 * @version 1.0
 */
public interface BitMap {

	public final int Bit_32 = 32;
	public final int Bit_64 = 64;

	public void add(long i);

	public boolean contains(long i);

	public void remove(long i);
}