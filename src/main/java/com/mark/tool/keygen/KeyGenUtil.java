package com.mark.tool.keygen;

import java.util.Random;
import java.util.UUID;

/**
 * 激活码生成工具-不重复性
 * @author maliqiang
 * @since 2017年2月4日
 * @version 1.0
 * 
 *
 */
public class KeyGenUtil {
	/**
	 * generator key
	 * @return
	 */
	public static synchronized String keyGen(){
		String key = UUID.randomUUID().toString();
		String encrypt_key  = key.substring(4, key.length()-8).toUpperCase();
		Integer oldChar = new Random().nextInt(10);
		String newChar =Character.toString((char)getCharNum(65, 90)) ;
		return encrypt_key.replace(oldChar.toString(), newChar);
		
	}
	
	/**
	 * generator a random integer between a and b
	 * @param a
	 * @param b
	 * @return
	 */
	public static synchronized int getCharNum(Integer a,Integer b){
		Integer charNum = (int) ((Math.random()*(b-a+1))+a);
		return charNum;
	}
	
}
