package com.mark.tool.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import com.mark.tool.constant.Constant;

public class IdCardUtil {

	/**
	 * 返回性别F:女、M：男
	 * 
	 * @param idCard
	 * @return
	 */
	public static String getSex(String idCard) {

		String sGender = "";
		if (idCard.length() == IdCardVerifyHelper.CHINA_ID_MIN_LENGTH) {
			idCard = IdCardVerifyHelper.conver15CardTo18(idCard);
		}
		String sCardNum = idCard.substring(16, 17);
		if (Integer.parseInt(sCardNum) % 2 != 0) {
			sGender = Constant.Sex.M.toString();
		} else {
			sGender = Constant.Sex.F.toString();
		}
		return sGender;
	}

	/**
	 * 根据身份编号获取生日
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(yyyyMMdd)
	 */
	public static String getBirthByIdCard(String idCard) {
		Integer len = idCard.length();
		if (len < IdCardVerifyHelper.CHINA_ID_MIN_LENGTH) {
			return null;
		} else if (len == IdCardVerifyHelper.CHINA_ID_MIN_LENGTH) {
			idCard = IdCardVerifyHelper.conver15CardTo18(idCard);
		}
		return idCard.substring(6, 14);
	}

	/**
	 * @根据日期以及日期格式，获取生日日期
	 * 
	 * @return String
	 */
	public static Date getDate(String date, String format) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		SimpleDateFormat sf1 = new SimpleDateFormat("yyyyMMdd");
		try {
			return simpleDateFormat.parse(simpleDateFormat.format((sf1.parse(date))));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 根据身份证号，自动获取对应的星座
	 * 
	 * @param idCard
	 *            身份证号码
	 * @return 星座
	 */
	public static String getConstellationById(String idCard) {
		if (!IdCardVerifyHelper.validateCard(idCard)) {
			return "";
		}
		int month = getMonthByIdCard(idCard);
		int day = getDateByIdCard(idCard);

		return getConstellation(month, day);
	}

	/**
	 * 根据生日返回星座
	 * 
	 * @param date
	 *            格式为yyyyMMdd、yyyy-MM-dd
	 * @return
	 * @throws Exception
	 */
	public static String getConstellationDate(String dateStr) throws Exception {
		if (StringUtil.isEmpty(dateStr)) {
			return "";
		}
		dateStr = DateUtil.formatDateToString(null,DateUtil.parseStringToDate(dateStr, Constant.DATE_FORMAT_YYYY_MM_DD), Constant.DATE_FORMAT_YYYY_MM_DD);
		dateStr = dateStr.replaceAll("-", "").trim();
		if (dateStr.length() != 8) {
			return "";
		}
		return getConstellation(Integer.parseInt(dateStr.substring(4, 6)), Integer.parseInt(dateStr.substring(6, 8)));
	}

	/**
	 * 判断星座
	 * 
	 * @param month
	 * @param day
	 * @return
	 */
	private static String getConstellation(int month, int day) {
		String strValue = "";
		if ((month == 1 && day >= 20) || (month == 2 && day <= 18)) {
			/* 水瓶座 */
			strValue = Constant.CONSTELLATION.AQUARIAN.getCode();
		} else if ((month == 2 && day >= 19) || (month == 3 && day <= 20)) {
			/* 双鱼座 */
			strValue = Constant.CONSTELLATION.PISCES.getCode();
		} else if ((month == 3 && day > 20) || (month == 4 && day <= 19)) {
			/* 白羊座 */
			strValue = Constant.CONSTELLATION.ARIES.getCode();
		} else if ((month == 4 && day >= 20) || (month == 5 && day <= 20)) {
			/* 金牛座 */
			strValue = Constant.CONSTELLATION.TAURUS.getCode();
		} else if ((month == 5 && day >= 21) || (month == 6 && day <= 21)) {
			/* 双子座 */
			strValue = Constant.CONSTELLATION.GEMINI.getCode();
		} else if ((month == 6 && day > 21) || (month == 7 && day <= 22)) {
			/* 巨蟹座 */
			strValue = Constant.CONSTELLATION.CANCER.getCode();
		} else if ((month == 7 && day > 22) || (month == 8 && day <= 22)) {
			/* 狮子座 */
			strValue = Constant.CONSTELLATION.LEONIS.getCode();
		} else if ((month == 8 && day >= 23) || (month == 9 && day <= 22)) {
			/* 处女座 */
			strValue = Constant.CONSTELLATION.VIRGO.getCode();
		} else if ((month == 9 && day >= 23) || (month == 10 && day <= 23)) {
			/* 天秤座 */
			strValue = Constant.CONSTELLATION.LIBRA.getCode();
		} else if ((month == 10 && day > 23) || (month == 11 && day <= 22)) {
			/* 天蝎座 */
			strValue = Constant.CONSTELLATION.SCORPIO.getCode();
		} else if ((month == 11 && day > 22) || (month == 12 && day <= 21)) {
			/* 射手座 */
			strValue = Constant.CONSTELLATION.SAGITTARIUS.getCode();
		} else if ((month == 12 && day > 21) || (month == 1 && day <= 19)) {
			/* 魔羯座 */
			strValue = Constant.CONSTELLATION.CAPRICORNUS.getCode();
		}
		return strValue;
	}

	/**
	 * 根据身份证号码获得年龄
	 * 
	 * @param idCard
	 * @return
	 * @throws Exception
	 */
	public static Short getAge(String idCard) {
		Date birthDay = getDate(getBirthByIdCard(idCard), "yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		if (cal.before(birthDay)) {
			return null;
		}

		int yearNow = cal.get(Calendar.YEAR);
		int monthNow = cal.get(Calendar.MONTH);
		int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
		cal.setTime(birthDay);

		int yearBirth = cal.get(Calendar.YEAR);
		int monthBirth = cal.get(Calendar.MONTH);
		int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

		int age = yearNow - yearBirth;
		if (monthNow <= monthBirth) {
			if (monthNow == monthBirth) {
				// monthNow==monthBirth
				if (dayOfMonthNow < dayOfMonthBirth) {
					age--;
				}
			} else {
				// monthNow>monthBirth
				age--;
			}
		}
		return age <= 0 ? null : new Short(String.valueOf(age));
	}

	/**
	 * 根据身份编号获取生日年
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(yyyy)
	 */
	public static Short getYearByIdCard(String idCard) {
		Integer len = idCard.length();
		if (len < IdCardVerifyHelper.CHINA_ID_MIN_LENGTH) {
			return null;
		} else if (len == IdCardVerifyHelper.CHINA_ID_MIN_LENGTH) {
			idCard = IdCardVerifyHelper.conver15CardTo18(idCard);
		}
		return Short.valueOf(idCard.substring(6, 10));
	}

	/**
	 * 根据身份编号获取生日月
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(MM)
	 */
	public static Short getMonthByIdCard(String idCard) {
		Integer len = idCard.length();
		if (len < IdCardVerifyHelper.CHINA_ID_MIN_LENGTH) {
			return null;
		} else if (len == IdCardVerifyHelper.CHINA_ID_MIN_LENGTH) {
			idCard = IdCardVerifyHelper.conver15CardTo18(idCard);
		}
		return Short.valueOf(idCard.substring(10, 12));
	}

	/**
	 * 根据身份证号，自动获取对应的生肖
	 * 
	 * @param idCard
	 *            身份证号码
	 * @return 生肖
	 */
	public static String getZodiacById(String idCard) { // 根据身份证号，自动返回对应的生肖
		// String sSX[] = { "猪", "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴",
		// "鸡", "狗" };
		String sSX[] = { ZODIAC.BO.getCode(), ZODIAC.RA.getCode(), ZODIAC.OX.getCode(), ZODIAC.TI.getCode(), ZODIAC.HA.getCode(), ZODIAC.DR.getCode(), ZODIAC.SN.getCode(), ZODIAC.HO.getCode(), ZODIAC.SH.getCode(), ZODIAC.MO.getCode(), ZODIAC.CA.getCode(), ZODIAC.DO.getCode() };
		int year = getYearByIdCard(idCard);
		int end = 3;
		int x = (year - end) % 12;
		String retValue = "";
		retValue = sSX[x];
		return retValue;
	}

	/**
	 * 根据身份编号获取生日天
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(dd)
	 */
	public static Short getDateByIdCard(String idCard) {
		Integer len = idCard.length();
		if (len < IdCardVerifyHelper.CHINA_ID_MIN_LENGTH) {
			return null;
		} else if (len == IdCardVerifyHelper.CHINA_ID_MIN_LENGTH) {
			idCard = IdCardVerifyHelper.conver15CardTo18(idCard);
		}
		return Short.valueOf(idCard.substring(12, 14));
	}

	/**
	 * 身份证号码显示为3*****************1
	 * 
	 * @param idCard
	 * @return
	 */
	public static String convertIdCard(String idCard) {
		if (!StringUtil.isNullOrEmpty(idCard) && idCard.length() >= 15) {
			if (idCard.length() == 18) {
				idCard = idCard.replaceAll("(\\d{1})\\d{16}([\\d{1}\\w{1}])", "$1****************$2");
			}else if (idCard.length() == 15) {
				idCard = idCard.replaceAll("(\\d{1})\\d{13}(\\d{1})", "$1*************$2");
			}
		}
		return idCard;
	}

	/**
	 * 指定字符串替换为*号
	 * 
	 * @param str
	 * @return
	 */
	public static String replaceStr(String str) {
		return str.replaceAll(".", "*");
	}

	/**
	 * 手机号码显示为138****1234
	 * 
	 * @param mobile
	 * @return
	 */
	public static String convertMobile(String mobile) {
		if (!StringUtil.isNullOrEmpty(mobile) && mobile.length() == 11) {
			mobile = mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
		}
		return mobile;
	}
	
	/**
	 * 生肖
	 */
		public enum ZODIAC {

			RA("RA", "鼠"), OX("OX", "牛"), TI("TI", "虎"), HA("HA", "兔"), DR("DR",
					"龙"), SN("SN", "蛇"), HO("HO", "马"), SH("SH", "羊"), MO("MO", "猴"), CA(
					"CA", "鸡"), DO("DO", "狗"), BO("BO", "猪");
			private final String code;

			private final String name;

			private ZODIAC(String code, String name) {
				this.code = code;
				this.name = name;
			}

			public String getCode() {
				return code;
			}


			public String getName() {
				return name;
			}


			public static ZODIAC getEnumByCode(String code) {
				for (ZODIAC z : ZODIAC.values()) {
					if (z.getCode().equals(code)) {
						return z;
					}
				}
				return null;
			}
		}
}
