package com.mark.tool.constant;


/**
 * 常量类
 * @author maliqiang
 * @version 1.0
 * @since 2016年11月22日
 *
 */
public class Constant {

	public final static int CHINA_ID_MIN_LENGTH = 15;
	
	public static String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	/**
	 *
	 * M:男，F：女
	 *影响：1.APP，2.核心交易
	 */
	public enum Sex {
		F, M
	}
	
	/**
	 *
	 * @title 星座
	 */
	public enum CONSTELLATION {
		AQUARIAN("AQU", "水瓶座"), PISCES("PIS", "双鱼座"), ARIES("ARI", "白羊座"), TAURUS(
				"TAU", "金牛座"), GEMINI("GEM", "双子座"), CANCER("CAN", "巨蟹座"), LEONIS(
				"LEO", "狮子座"), VIRGO("VIR", "处女座"), LIBRA("LIB", "天秤座"), SCORPIO(
				"SCO", "天蝎座"), SAGITTARIUS("SAG", "射手座"), CAPRICORNUS("CAP",
				"摩羯座");
		private final String code;

		private final String name;

		private CONSTELLATION(String code, String name) {
			this.code = code;
			this.name = name;
		}

		public String getCode() {
			return code;
		}


		public String getName() {
			return name;
		}


		public static CONSTELLATION getEnumByCode(String code) {
			for (CONSTELLATION c : CONSTELLATION.values()) {
				if (c.getCode().equals(code)) {
					return c;
				}
			}
			return null;
		}
	}
}
