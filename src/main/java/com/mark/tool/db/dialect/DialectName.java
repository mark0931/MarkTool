package com.mark.tool.db.dialect;

/**
 * 方言名
 * @author Looly
 *
 */
public enum DialectName {
	ANSI, MYSQL, ORACLE, POSTGREESQL, SQLITE3
}
