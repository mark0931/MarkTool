package com.mark.tool.db.dialect.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

import com.mark.tool.db.DbUtil;
import com.mark.tool.db.Entity;
import com.mark.tool.db.Page;
import com.mark.tool.db.dialect.DialectName;
import com.mark.tool.db.sql.Order;
import com.mark.tool.db.sql.SqlBuilder;
import com.mark.tool.db.sql.Wrapper;
import com.mark.tool.db.sql.SqlBuilder.LogicalOperator;
import com.mark.tool.exceptions.DbRuntimeException;
import com.mark.tool.util.StringUtil;

/**
 * Oracle 方言
 * @author loolly
 *
 */
public class OracleDialect extends AnsiSqlDialect{
	
	public OracleDialect() {
		wrapper = new Wrapper('"');	//Oracle所有字段名用双引号包围，防止字段名或表名与系统关键字冲突
	}
	
	@Override
	public PreparedStatement psForPage(Connection conn, Collection<String> fields, Entity where, Page page) throws SQLException {
		//验证
		if(where == null || StringUtil.isBlank(where.getTableName())) {
			throw new DbRuntimeException("Table name is null !");
		}
		
		final SqlBuilder find = SqlBuilder.create(wrapper)
				.select(fields)
				.from(where.getTableName())
				.where(LogicalOperator.AND, DbUtil.buildConditions(where));
		
		final Order[] orders = page.getOrders();
		if(null != orders){
			find.orderBy(orders);
		}
		
		int[] startEnd = page.getStartEnd();
		final SqlBuilder sql = SqlBuilder.create(wrapper);
		sql.append("SELECT * FROM ( SELECT row_.*, rownum rownum_ from ( ")
			.append(find)
			.append(" ) row_ where rownum <= ").append(startEnd[1])
			.append(") table_alias")
			.append(" where table_alias.rownum_ >= ").append(startEnd[0]);
		
		final PreparedStatement ps = conn.prepareStatement(sql.build());
		DbUtil.fillParams(ps, find.getParamValues());
		return ps;
	}
	
	@Override
	public DialectName dialectName() {
		return DialectName.ORACLE;
	}
}
