package com.mark.tool.demo.http;

import com.mark.tool.http.HttpUtil;
import com.mark.tool.util.FileUtil;

/**
 * 下载样例
 * @author Looly
 *
 */
public class DownloadDemo {
	public static void main(String[] args) {
		// 下载文件
		long size = HttpUtil.downloadFile("https://www.baidu.com/", FileUtil.file("e:/"));
		System.out.println("Download size: " + size);
	}
}
