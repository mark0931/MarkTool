package com.mark.tool.demo.http;

import java.util.HashMap;

import com.mark.tool.http.HttpUtil;
import com.mark.tool.lang.Console;

/**
 * Post请求样例
 * @author Looly
 *
 */
public class HttpPostDemo {
	public static void main(String[] args) {
		HashMap<String, Object> paramMap = new HashMap<>();
		paramMap.put("city", "北京");
		String result1 = HttpUtil.post("http://wthrcdn.etouch.cn/weather_mini", paramMap);
		Console.log(result1);
	}
}
