package com.mark.tool.demo;

import java.io.IOException;

import com.mark.tool.util.FileUtil;
import com.mark.tool.util.SecurityUtil;

/**
 * 安全工具类Demo
 * @author Looly
 *
 */
public class SecureUtilDemo {
	public static void main(String[] args) throws IOException {
		byte[] bytes = FileUtil.readBytes(FileUtil.file("d:\\aaa.png"));
		byte[] base64 = SecurityUtil.base64(bytes, true);
		byte[] decodeBase64 = SecurityUtil.decodeBase64(base64);
		FileUtil.writeBytes(decodeBase64, "d:\\bbb.png");
		System.out.println("OK");
	}
}
