package com.mark.tool.test;

import com.mark.tool.dto.CompanyInfo;
import com.mark.tool.spider.SpiderHtmlUtil;

public class SpiderTest {
	public static String encodeing = "UTF-8";

	public static void main(String[] args) throws Exception {
		String keyWord = "唯品会";
		/**
		 * 根据关键字查询职位
		 */
		String rsp = SpiderHtmlUtil.requestUrlByKeyWord(keyWord);
		/**
		 * 打开职位搜索结果页
		 */
		String href = SpiderHtmlUtil.getDesInfoUrl(rsp);
		
//		String info = SpiderHtmlUtil.requestUrl(href);
//		System.out.println(info);
//		String type = SpiderHtmlUtil.getCompanyKeyWord(info);		
//		System.out.println(type);
		CompanyInfo res = SpiderHtmlUtil.getCompanyInfo(keyWord);
		System.out.println("公司名称："+res.getCompName());
		System.out.println("人数："+res.getEmployeeCnt());
		System.out.println("类型："+res.getType());
		System.out.println("行业："+res.getIndustry());
	}

}
