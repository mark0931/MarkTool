package com.mark.tool.test;

import java.util.ArrayList;
import java.util.List;

import com.mark.tool.constant.WeightCategory;
import com.mark.tool.randomalgorithm.WeightRandom;

public class TestGroup {
	static List<WeightCategory> categorys = new ArrayList<WeightCategory>();

	public static List<WeightCategory> initData() {
		WeightCategory wc1 = new WeightCategory("test1", 20);
		WeightCategory wc2 = new WeightCategory("sample", 60);
		WeightCategory wc3 = new WeightCategory("test2", 20);
		categorys.add(wc1);
		categorys.add(wc2);
		categorys.add(wc3);
		return  categorys;
	}
	
	public static void main(String[] args) {
		categorys = initData();
		Integer a = 0, b = 0, c = 0;
		for (int i = 0; i < 100000; i++) {
			String result = WeightRandom.groupUser(categorys);
			switch (result) {
			case "test1":
				a++;
				break;
			case "sample":
				b++;
				break;

			case "test2":
				c++;
				break;
			}
		}
		System.out.println("a=" + a);
		System.out.println("b=" + b);
		System.out.println("c=" + c);
	}
}
