package com.mark.tool.test;
import com.mark.tool.util.SecurityUtil;
 /**
  * DES加解密测试
  * @author maliqiang
  *
  */
public class DesTest {
 
    public static void main(String[] args) throws Exception {
        String data = "uxun";
        String key = "mark!@#$%";
        System.err.println("加密："+SecurityUtil.encrypt(data, key));
        System.err.println("解密："+SecurityUtil.decrypt(SecurityUtil.encrypt(data, key), key));
 
    }
}